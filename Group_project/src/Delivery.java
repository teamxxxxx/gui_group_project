
public class Delivery {
	
	private int delivery_id;
	private int cus_id;
	private String cus_First_Name;
	private String cus_Last_Name;
	private String cus_Address_Line_1;
	private int driver_id;
	
	public int getDelivery_id() {
		return delivery_id;
	}

	public void setDelivery_id(int i) {
		this.delivery_id = i;
	}

	public int getCus_id() {
		return cus_id;
	}

	public void setCus_id(int i) {
		this.cus_id = i;
	}

	public String getCus_First_Name() {
		return cus_First_Name;
	}

	public void setCus_First_Name(String s) {
		this.cus_First_Name = s;
	}

	public String getCus_Last_Name() {
		return cus_Last_Name;
	}

	public void setCus_Last_Name(String s) {
		this.cus_Last_Name = s;
	}

	public String getCus_Address_Line_1() {
		return cus_Address_Line_1;
	}

	public void setCus_Address_Line_1(String s) {
		this.cus_Address_Line_1 = s;
	}

	public int getDriver_id() {
		return driver_id;
	}

	public void setDriver_id(int i) {
		this.driver_id = i;
	}

	public Delivery()
	{
		
	}
	
	public void printDelivery()
	{
		
	}

}
